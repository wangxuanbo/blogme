package com.xinQing.blogme.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * Created by null on 2017/7/31.
 */
@Getter
@Setter
@Table(name = "t_article")
public class Article extends Entity {

    @Column
    private String userId;

    @Column
    private String name;

    @Column
    private String content;

    @Column
    private String description;

}