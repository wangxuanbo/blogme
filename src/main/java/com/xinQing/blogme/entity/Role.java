package com.xinQing.blogme.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * Created by null on 2017/7/31.
 */
@Getter
@Setter
@Table(name = "t_role")
public class Role extends Entity {

    @Column
    private String name;

    @Column
    private String description;

}