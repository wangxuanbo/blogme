package com.xinQing.blogme.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by null on 2017/7/31.
 */
@Getter
@Setter
public class Entity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    private String id;

    @Column
    private Date createTime;

    @Column
    private Date updateTime;

}