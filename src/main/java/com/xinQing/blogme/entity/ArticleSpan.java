package com.xinQing.blogme.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * Created by null on 2017/8/1.
 */
@Getter
@Setter
@Table(name = "t_article_span")
public class ArticleSpan extends Entity {

    @Column
    private String articleId;

    @Column
    private String spanId;

}