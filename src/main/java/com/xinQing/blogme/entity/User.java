package com.xinQing.blogme.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * Created by null on 2017/7/31.
 */
@Getter
@Setter
@Table(name = "t_user")
public class User extends Entity {

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String email;

    @Column
    private String nickname;

    @Column
    private String roleId;

}