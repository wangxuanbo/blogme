package com.xinQing.blogme.util;

import com.xinQing.blogme.entity.Entity;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 继承该mapper获得通用方法
 *
 * Created by null on 2017/3/8.
 *
 * 参考:https://github.com/abel533/MyBatis-Spring-Boot/blob/master/src/main/java/tk/mybatis/springboot/util/MyMapper.java
 */
public interface MyMapper<T extends Entity> extends Mapper<T>, MySqlMapper<T> {
    // TODO
    // FIXME 特别注意，该接口不能被扫描到，否则会出错
}