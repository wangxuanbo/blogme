package com.xinQing.blogme.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * Created by null on 2017/8/1.
 */
@Getter
@Setter
public class Result<T> {

    private Integer code;

    private String message;

    private T data;

    private Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> Result<T> ok(T data) {
        return new Result<>(HttpStatus.OK.value(), null, data);
    }

    public static <T> Result<T> ok(String message, T data) {
        return new Result<>(HttpStatus.OK.value(), message, data);
    }

    public static <T> Result<T> fail(Integer code, String message, T data) {
        return new Result<>(code, message, data);
    }

    public static <T> Result<T> fail(String message, T data) {
        return new Result<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, data);
    }
}