package com.xinQing.blogme.util;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 封装分页信息
 *
 * Created by null on 2016/11/10.
 *
 * ##############################################
 * 修改记录：
 * 添加ofPageInfo方法修复分页查询时，数据为null时当前页返回为0的bug(修改为1)
 *
 * fix by null on 2017/4/5.
 * ##############################################
 */
public class Pageable {

    // 当前页
    private Integer current = 1;

    // 每页显示多少条数据
    private Integer size = 20;

    public Pageable() {
    }

    public Pageable(Integer current, Integer size) {
        this.current = current;
        this.size = size;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * 调用该方法，紧挨着的查询方法会分页查询(sql加上limit n, m)
     */
    public void startPage() {
        PageHelper.startPage(this.getCurrent(), this.getSize());
    }

    /**
     * 返回分页信息，修复返回当前页为0的情况
     *
     * @param list 分页的数据
     * @param <T> DTO
     * @return PageInfo
     */
    public <T> PageInfo<T> ofPage(List<T> list) {
        PageInfo<T> pageInfo = new PageInfo<>(list);
        if (pageInfo.getPageNum() == 0) {
            pageInfo.setPageNum(1);
        }
        return pageInfo;
    }
}