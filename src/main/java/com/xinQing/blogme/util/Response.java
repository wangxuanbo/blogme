package com.xinQing.blogme.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * HttpServletResponse工具类
 *
 * Created by null on 2017/3/6.
 */
public class Response {

    public static void writeJsonAndFlush(HttpServletResponse response,  Result<?> result) throws IOException {
        response.setCharacterEncoding("UTF-8");
        // 设置返回json
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        // 认证成功
        writer.append(JSONObject.toJSONString(result));
        writer.flush();
    }

}