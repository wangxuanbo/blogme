package com.xinQing.blogme.dao;

import com.xinQing.blogme.entity.Role;
import com.xinQing.blogme.util.MyMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by null on 2017/7/31.
 */
@Mapper
public interface RoleDao extends MyMapper<Role> {
}