package com.xinQing.blogme.lucene;

import com.xinQing.blogme.entity.Article;

import java.util.List;

/**
 * Created by null on 2017/8/7.
 */
public interface LuceneService {

    void createIndex(Article article);

    List<Article> search(String word);

    void deleteIndex(String id);

    void updateIndex(Article article);

}
