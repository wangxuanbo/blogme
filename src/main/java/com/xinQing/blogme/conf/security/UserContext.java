package com.xinQing.blogme.conf.security;

import com.xinQing.blogme.entity.Role;
import com.xinQing.blogme.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户上下文
 *
 * Created by null on 2017/8/7.
 */
@Data
@AllArgsConstructor
public class UserContext implements Serializable{

    private User user;

    private Role role;

}
