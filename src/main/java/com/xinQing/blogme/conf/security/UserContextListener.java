package com.xinQing.blogme.conf.security;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

/**
 * ServletRequestListener
 *
 * Created by null on 2017/8/7.
 */
@Slf4j
@WebListener
public class UserContextListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        UserContextHolder.clear();
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        log.debug("initial request {}", sre.getServletRequest());
    }
}
