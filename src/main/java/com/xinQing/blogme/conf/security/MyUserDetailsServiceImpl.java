package com.xinQing.blogme.conf.security;

import java.util.ArrayList;
import java.util.Collection;

import com.xinQing.blogme.entity.Role;
import com.xinQing.blogme.entity.User;
import com.xinQing.blogme.service.RoleService;
import com.xinQing.blogme.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Spring Security获取用户信息，从而进行身份认证
 * <p>
 * Created by null on 2017/2/22.
 */
public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("账号不存在");
        }
        Role role = roleService.select(user.getRoleId());
        if (role == null) {
            throw new UsernameNotFoundException("账号还没有授予权限");
        }
        // 将用户上下文放入UserContextHolder
        UserContextHolder.setUserContext(new UserContext(user, role));
        return new MyUserDetails(user, getAuthorities(role));
    }

    /**
     * 获取权限
     * 我这里用户跟角色是一对一
     *
     * @param role 角色
     * @return Collection<GrantedAuthority>
     */
    private Collection<GrantedAuthority> getAuthorities(Role role) {
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getName());
        grantedAuthorities.add(grantedAuthority);
        return grantedAuthorities;
    }

}