package com.xinQing.blogme.conf.security;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by null on 2017/8/2.
 */
@Getter
@Setter
public class JWTUser implements Serializable {

    private String username;

    private String password;

}
