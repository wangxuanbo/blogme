package com.xinQing.blogme.conf.exception;

import com.xinQing.blogme.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by null on 2017/8/1.
 */
@Slf4j
@RestControllerAdvice
public class GlobalValidatorConfig {

    /*----------------------------------------------------
     * 参数校验 start
     ----------------------------------------------------*/
    @ExceptionHandler(ConstraintViolationException.class)
    public Result<List<ValidatorError>> constraintViolationExceptionHandle(ConstraintViolationException e) {
        log.warn("catch ConstraintViolationException", e);
        return Result.fail(415, "参数错误", getValidatorErrors(e));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result<List<ValidatorError>> methodArgumentNotValidExceptionHandle(MethodArgumentNotValidException e) {
        log.warn("catch MethodArgumentNotValidException", e);
        return Result.fail(415, "参数错误", getValidatorErrors(e.getBindingResult()));
    }

    @ExceptionHandler(BindException.class)
    public Result<List<ValidatorError>> bindExceptionHandle(BindException e) {
        log.warn("catch BindException", e);
        return Result.fail(415, "参数错误", getValidatorErrors(e));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Result<ValidatorError> missingServletRequestParameterExceptionHandle(MissingServletRequestParameterException e) {
        log.warn("catch MissingServletRequestParameterException", e);
        return Result.fail(415, "参数错误", new ValidatorError(e.getParameterName(), e.getMessage()));
    }
    /*----------------------------------------------------
     * 参数校验 end
     ----------------------------------------------------*/

    @ExceptionHandler(NoHandlerFoundException.class)
    public Result NoHandlerFoundExceptionHandle(NoHandlerFoundException e) {
        log.warn("catch NoHandlerFoundException", e);
        return Result.fail(404, e.getMessage(), null);
    }

    @ExceptionHandler(Exception.class)
    public Result<String> unknownException(Exception e) {
        log.warn("catch unknownException", e);
        return Result.fail(e.getMessage(), null);
    }

    private List<ValidatorError> getValidatorErrors(ConstraintViolationException e) {
        return e.getConstraintViolations()
                .stream()
                .map(constraintViolation -> {
                    String[] pathSplit = constraintViolation.getPropertyPath().toString().split("\\.");
                    String field = pathSplit[1];
                    String errorInfo = constraintViolation.getMessage();
                    return new ValidatorError(field, errorInfo);
                }).collect(Collectors.toList());
    }

    private List<ValidatorError> getValidatorErrors(BindingResult bindingResult) {
        return bindingResult.getFieldErrors()
                .stream()
                .map(fieldError -> new ValidatorError(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());
    }

}