package com.xinQing.blogme.conf.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by null on 2017/8/1.
 */
@Getter
@Setter
@AllArgsConstructor
public class ValidatorError {

    private String field;

    private String errorInfo;

}