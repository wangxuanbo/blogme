package com.xinQing.blogme.constant;

import java.util.concurrent.TimeUnit;

/**
 * Created by null on 2017/8/6.
 */
public class CacheConstant {

    // 过期时间
    public static final long EXPIRE_TIME = 30;

    // 过期单位
    public static final TimeUnit EXPIRE_TIME_UNIT = TimeUnit.MINUTES;

}
