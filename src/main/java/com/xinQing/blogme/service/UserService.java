package com.xinQing.blogme.service;

import com.xinQing.blogme.entity.User;

/**
 * Created by null on 2017/7/31.
 */
public interface UserService extends BaseService<User, String> {

    User findByUsername(String username);

}