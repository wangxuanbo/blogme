package com.xinQing.blogme.service.impl;

import com.xinQing.blogme.entity.Article;
import com.xinQing.blogme.service.ArticleService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by null on 2017/7/31.
 */
@Service
@Transactional
public class ArticleServiceImpl extends BaseServiceImpl<Article, String>  implements ArticleService {

    @Value("${spring.redis.cacheKeyPrefix.article.id}")
    private String cacheKeyPrefix;

    @Override
    public String getCacheKeyPrefixOfId() {
        return cacheKeyPrefix;
    }
}