package com.xinQing.blogme.service.impl;

import com.xinQing.blogme.entity.Role;
import com.xinQing.blogme.service.RoleService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by null on 2017/7/31.
 */
@Service
@Transactional
public class RoleServiceImpl extends BaseServiceImpl<Role, String>  implements RoleService {

    @Value("${spring.redis.cacheKeyPrefix.role.id}")
    private String cacheKeyPrefix;

    @Override
    public String getCacheKeyPrefixOfId() {
        return cacheKeyPrefix;
    }
}