package com.xinQing.blogme.service.impl;

import com.xinQing.blogme.entity.Span;
import com.xinQing.blogme.service.SpanService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by null on 2017/7/31.
 */
@Service
@Transactional
public class SpanServiceImpl extends BaseServiceImpl<Span, String>  implements SpanService {

    @Value("${spring.redis.cacheKeyPrefix.span.id}")
    private String cacheKeyPrefix;

    @Override
    public String getCacheKeyPrefixOfId() {
        return cacheKeyPrefix;
    }
}