package com.xinQing.blogme.service;

import com.github.pagehelper.PageInfo;
import com.xinQing.blogme.entity.Entity;
import com.xinQing.blogme.util.Pageable;
import tk.mybatis.mapper.entity.Example;

import java.io.Serializable;
import java.util.List;

/**
 * BaseService 定义基本的业务逻辑
 * T:   实体类
 * ID:  实体类的主键
 * 注意:针对单表操作
 *
 * Created by null on 2016/11/7.
 *
 * ##############################################
 * 修改记录：
 * (1）增加批量插入
 * (2) 去掉排序
 *
 * fix by null on 2017/3/8.
 * ##############################################
 *
 * 修改记录：
 * (1）业务层添加缓存
 *
 * fix by null on 2017/8/6.
 * ##############################################
 */
public interface BaseService<T extends Entity, ID extends Serializable> {

    /**
     * 插入一条数据
     *
     * @param t 要插入的实体对象
     * @return 该操作影响数据库的行数
     */
    int insert(T t);

    /**
     * 批量插入多条记录
     *
     * @param list List
     * @return 插入记录的条数
     */
    int insertList(List<T> list);

    /**
     * 根据实体信息，查询记录
     *
     * @param t 要查询的实体信息
     * @return 查询的记录
     */
    List<T> selectMany(T t);

    /**
     * 根据实体信息，查询记录
     *
     * @param t 要查询的实体信息
     * @return 查询的记录
     */
    T selectOne(T t);

    /**
     * 根据id查询
     *
     * @param id 主键
     * @return 查询的记录
     */
    T select(ID id);

    /**
     * 查询所有记录
     *
     * @return 所有记录
     */
    List<T> selectAll();

    /**
     * Example查询
     *
     * @return 满足条件的记录
     */
    List<T> select(Example example);

    /**
     * 根据实体更新记录（根据id进行更新）
     *
     * @param t 实体
     * @return 该操作影响数据库的行数
     */
    int update(T t);

    /**
     * 根据id删除记录
     *
     * @param id 记录的id
     * @return 该操作影响数据库的行数
     */
    int remove(ID id);

    /**
     * 分页获取信息
     * 如果实体不为null,则根据实体的属性进行精确查询
     * 如果Pageable为null,则默认查询第一页,每页显示10条数据
     *
     * 修订：不支持排序
     *
     * @param t 实体
     * @param pageable 分页信息
     * @return PageInfo<T>
     */
    PageInfo<T> select(T t, Pageable pageable);

    /**
     * 获取换缓存key的前缀
     *
     * @return
     */
    String getCacheKeyPrefixOfId();

}