package com.xinQing.blogme.service;

import com.xinQing.blogme.entity.Role;

/**
 * Created by null on 2017/7/31.
 */
public interface RoleService extends BaseService<Role, String> {
}