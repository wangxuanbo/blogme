package com.xinQing.blogme.service;

import com.xinQing.blogme.entity.Article;

/**
 * Created by null on 2017/7/31.
 */
public interface ArticleService extends BaseService<Article, String> {
}