-- ----------------------------
-- 角色
-- ----------------------------
CREATE TABLE IF NOT EXISTS `t_role` (
  `id` VARCHAR(100) NOT NULL PRIMARY KEY,
  `name` VARCHAR(50) UNIQUE NOT NULL,
  `description` VARCHAR(100) DEFAULT NULL,
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 用户
-- ----------------------------
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` VARCHAR(100) NOT NULL PRIMARY KEY,
  `username` VARCHAR(50) UNIQUE NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `email` VARCHAR(50) UNIQUE NOT NULL,
  `nickname` VARCHAR(20) NOT NULL,
  `role_id` VARCHAR(100) NOT NULL,
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 文章
-- ----------------------------
CREATE TABLE IF NOT EXISTS `t_article` (
  `id` VARCHAR(100) NOT NULL PRIMARY KEY,
  `user_id` VARCHAR(100) NOT NULL,
  `name` VARCHAR(200) NOT NULL,
  `content` TEXT NOT NULL,
  `description` VARCHAR(600) NOT NULL,
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 标签
-- ----------------------------
CREATE TABLE IF NOT EXISTS `t_span` (
  `id` VARCHAR(100) NOT NULL PRIMARY KEY,
  `name` VARCHAR(50) UNIQUE NOT NULL,
  `description` VARCHAR(200) NOT NULL,
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 文章-标签
-- ----------------------------
CREATE TABLE IF NOT EXISTS `t_article_span` (
  `id` VARCHAR(100) NOT NULL PRIMARY KEY,
  `article_id` VARCHAR(100) NOT NULL,
  `span_id` VARCHAR(100) NOT NULL,
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;