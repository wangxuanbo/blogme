package com.xinQing.blogme.controller

import com.xinQing.blogme.entity.Span
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by null on 2017/8/9.
 */
@RestController
@RequestMapping("/span")
class SpanController: BaseController<Span, String>()