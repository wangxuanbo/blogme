package com.xinQing.blogme.controller

import com.github.pagehelper.PageInfo
import com.xinQing.blogme.entity.Entity
import com.xinQing.blogme.service.BaseService
import com.xinQing.blogme.util.Pageable
import com.xinQing.blogme.util.Result
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.io.Serializable

/**
 * 基础Controller
 *
 * Created by null on 2017/8/9.
 */
@RestController
abstract class BaseController<T: Entity, in ID: Serializable> {

    @Autowired
    private lateinit var baseService: BaseService<T, ID>

    @GetMapping("/page")
    fun page(t: T, pageable: Pageable): PageInfo<T> = baseService.select(t, pageable)

    @GetMapping("/{id}")
    fun show(@PathVariable id: ID): T = baseService.select(id)

    @PostMapping
    fun save(@RequestBody t: T): Result<Int> = Result.ok(baseService.insert(t))

    @PutMapping
    fun update(@RequestBody t: T): Result<Int> = Result.ok(baseService.update(t))

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: ID): Result<Int> = Result.ok(baseService.remove(id))

}