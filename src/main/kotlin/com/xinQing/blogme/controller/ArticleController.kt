package com.xinQing.blogme.controller

import com.xinQing.blogme.entity.Article
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * 文章
 *
 * Created by null on 2017/8/9.
 */
@RestController
@RequestMapping("/article")
class ArticleController: BaseController<Article, String>()