package com.xinQing.blogme

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.ServletComponentScan
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 * Created by null on 2017/8/9.
 */
@SpringBootApplication
@EnableTransactionManagement
@ServletComponentScan
class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}