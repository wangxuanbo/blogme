package com.xinQing.blogme.lucene;

import com.xinQing.blogme.entity.Article;
import com.xinQing.blogme.service.ArticleService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by null on 2017/8/7.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class LuceneServiceImplTest {

    @Autowired
    private LuceneService luceneService;

    @Autowired
    private ArticleService articleService;

    @Test
    public void testCreateIndex() {
        luceneService.createIndex(articleService.select("1"));
    }

    @Test
    public void testSearch() {
        luceneService.search("spring boot").forEach(article -> {
            log.info("-------------------------");
            log.info("id => {}", article.getId());
            log.info("name => {}", article.getName());
            log.info("content => {}", article.getContent());
            log.info("description => {}", article.getDescription());
            log.info("createTime => {}", article.getCreateTime());
            log.info("updateTime => {}", article.getUpdateTime());
        });
    }

    @Test
    public void testDelete() {
        luceneService.deleteIndex("1");
    }

    @Test
    public void testUpdate() {
        Article article = new Article();
        article.setId("1");
        article.setName("spring boot集成Lucene");
        article.setContent("# readme");
        article.setDescription("spring boot集成Lucene");
        luceneService.updateIndex(article);
    }

}
